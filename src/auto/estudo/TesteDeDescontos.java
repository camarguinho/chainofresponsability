package auto.estudo;

public class TesteDeDescontos {

	public static void main(String[] args) {
		CalculadorDeDesconto descontos = new CalculadorDeDesconto();
		
		Orcamento orcamento = new Orcamento(500.0);
		orcamento.adicionaItem(new Item("CANETA", 250.0));
		orcamento.adicionaItem(new Item("LAPIS", 95.0));
		orcamento.adicionaItem(new Item("BORRACHA", 300.0));
		orcamento.adicionaItem(new Item("ESTOJO", 150.0));
		orcamento.adicionaItem(new Item("MOCHILA", 400.0));
		orcamento.adicionaItem(new Item("LAPISEIRA", 400.0));
		
		double descontoFinal = descontos.calcula(orcamento);
				
		System.out.println(descontoFinal);
	}

}
